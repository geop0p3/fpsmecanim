﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Uniat.Theory
{
	public class OOP_Child : OOP_Father, OOP_Interfaz
	{
		
		void Update ()
		{
			if(Input.GetKeyDown(KeyCode.Space))
			{
				Conducir("Porque soy joven");
			}
		}
		
		public override void Conducir(string algunTexto)
		{
			//base.Conducir(algunTexto);
			Debug.Log("Conduzco Como gente joven " + algunTexto);
		}
		
		public void Disparar()
		{
			Debug.Log("Estoy Disparando");
		}
	}
}