﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coroutine : MonoBehaviour
{
	private IEnumerator printSomethingA;
	private IEnumerator printSomethingB;
	
	void Start () 
	{
		printSomethingA = Something(0.5f,"A");
		StartCoroutine(printSomethingA);
		printSomethingB = Something(1.5f,"B");
		StartCoroutine(printSomethingB);
	}
	
	void Update () 
	{
		if(Input.GetKeyDown(KeyCode.A))
		{
			StopCoroutine(printSomethingA);
		}
		
		if(Input.GetKeyDown(KeyCode.B))
		{
			StopCoroutine(printSomethingB);
		}
	}
	
	IEnumerator Something(float timeToWait, string name)
	{
		Debug.Log("("+name+")Start at " + Time.time);
		
		while(true)
		{
			yield return new WaitForSeconds(timeToWait);
			Debug.Log("("+name+")Doing something at " + Time.time);
		}
	}
}
