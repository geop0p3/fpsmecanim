﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Uniat.Theory
{
	public class OOP_Father : MonoBehaviour 
	{
		public int vida;
		public string nombre;
		
		public void RecibirDano()
		{
			//Get some damage
		}
		
		public virtual void Conducir(string algunTexto)
		{
			Debug.Log("Conduce como viejito");
		}
		
	}
}