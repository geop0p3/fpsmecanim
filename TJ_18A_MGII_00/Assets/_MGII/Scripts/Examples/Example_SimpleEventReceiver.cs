﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Uniat
{
	public class Example_SimpleEventReceiver : UniatBehaviour
	 {
		public GameObject objectToEnable;
		
		 public void ToggleObjectEnable()
		 {
		 	objectToEnable.SetActive(!objectToEnable.activeSelf);
		 }
		 
		 
		 public void DoSomethingRandom()
		 {
		 	Debug.Log("Hakuna Matatta");
		 }

	}
}