﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Uniat
{
	public class Spawnable : UniatBehaviour
	{

		public float life = 3;
		
		public void OnSpawn()//Nuevo Start Fake
		{
			Invoke("SelfDestruct",life);
		}
		
		void SelfDestruct()
		{
			//Ya no se llama a un Destroy, ahora reciclamos llamando a Despawn
			PoolManager.Despawn(gameObject);
		}
		
		public void OnDespawn()//Codigo seguro para dejar la instancia limpia y lista para ser utilizado de nuevo
		{
			CancelInvoke();
			gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
		}
	}
}