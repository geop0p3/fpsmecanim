﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Uniat
{
	public class UsingSingleton : UniatBehaviour
	{
		
	
		 void Start ()
		 {
			
		}
		
		 void Update () 
		 {
		 	if(Input.GetKeyDown(KeyCode.Space))
		 	{
		 		//Debug.Log( MySingleton.Instance.name );
		 		MySingleton.Instance.AddNewScore(Random.Range(0,100));
		 	}
		 	
		 	if(Input.GetKeyDown(KeyCode.Return))
		 	{
		 		MySingleton.Instance.DamagePlayer(Random.Range(0,100));
		 	}
		 }
		
		
	}
}