﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Uniat
{
	public class SubscribingToSingleton : UniatBehaviour
	{
		
		public UnityEngine.UI.Text displayScore;
		
		void Start()
		{
			MySingleton.Instance.Initialize();
		}
		
		void OnEnable()
		{
			MySingleton.addScoreHandler += GotNewScore;
		}
		
		void OnDisable()
		{
			MySingleton.addScoreHandler -= GotNewScore;
		}
		
		void OnDestroy()
		{
			MySingleton.addScoreHandler -= GotNewScore;
		}
		
		public void GotNewScore(int newScore)
		{
			displayScore.text = newScore.ToString();
		}
		
	}
}