﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Uniat
{
	public class SimpleShoot : UniatBehaviour
	{
		public GameObject bulletPrefab;
		private Transform lastBullet;
		
		public float force;
		public Transform spawnPoint;

		void Start () 
		{
			/*
			PoolManager.MakePool(bulletPrefab);
			
			PoolManager.SetPoolLimit(bulletPrefab,7);
			
			PoolManager.PreSpawn(bulletPrefab,20,true);
			*/
			
			//PoolManager.MakePool(bulletPrefab,20,5);
		}
		
		void Update () 
		{
			if(Input.GetKeyDown(KeyCode.Space))
			{
				lastBullet = PoolManager.Spawn(bulletPrefab,
					spawnPoint.position,
					spawnPoint.rotation);
					
				if(lastBullet)
				{
					lastBullet.gameObject.GetComponent<Rigidbody>().AddForce(spawnPoint.forward * force);	
				}
			}
		}

	}
}

