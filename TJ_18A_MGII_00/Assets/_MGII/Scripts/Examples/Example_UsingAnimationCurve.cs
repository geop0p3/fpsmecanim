﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Uniat
{
	public class Example_UsingAnimationCurve : UniatBehaviour
	 {
		 public AnimationCurve myCurve;
		 public float speed = 1;
		 public float magnitude = 1;
		void Start () 
		{
			
		}
		
		 void Update () 
		 {
		 	
			 SetPosition_X( myCurve.Evaluate((Time.time*speed)%1) * magnitude );
			
		}
	}
}