﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Uniat
{
	public class SubscribingToSingleton1 : UniatBehaviour
	{
		
		public Vector3 movementAxis;
		public float frequency;
		public float amplitude;
		
		void Start()
		{
			MySingleton.Instance.Initialize();
		}
		
		void OnEnable()
		{
			MySingleton.damagePlayerHandler += OnDamagePlayer;
		}
		
		void OnDisable()
		{
			MySingleton.damagePlayerHandler -= OnDamagePlayer;
		}
		
		void OnDestroy()
		{
			MySingleton.damagePlayerHandler -= OnDamagePlayer;
		}
		
		public void OnDamagePlayer(int damageAmount)
		{
			//*(damageAmount*0.01f)
			iTween.ShakePosition(gameObject, movementAxis*amplitude,frequency);
		}
		
	}
}