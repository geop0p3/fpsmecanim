﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Uniat
{
	public class Mecanim_Simple_Inputs : MonoBehaviour 
	{
		private Animator m_animator;
		private Vector2 m_direction;
		public Vector2 direction
		{
			set { m_direction = value;
				m_animator.SetFloat("Horizontal", m_direction.x);
				m_animator.SetFloat("Vertical", m_direction.y);
			}
			get{ return m_direction;}
		}
		
		public bool setDirection;
		public bool setJump;
		public float m_speed;
		
		void Start () 
		{
			m_animator = GetComponent<Animator>();
		}
		
		void Update()
		{
			if(setDirection)
			{
				direction = new Vector2(Input.GetAxis("Horizontal"),Input.GetAxis("Vertical"));
				transform.Translate(new Vector3(direction.x,0,direction.y) * m_speed * Time.deltaTime, Space.World);	
			}
			
			if(setJump)
			{
				if(Input.GetKeyDown(KeyCode.Space))
				{
					m_animator.SetTrigger("Jump");
				}
			}
		}
		
	}
}

