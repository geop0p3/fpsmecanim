﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Uniat
{
	public class Mecanim_IKs : MonoBehaviour {
	
		public Animator animator;
		
		private GameObject pickUpTarget;
		private float pickUpTargetWeight = 1;
		
		public Transform[] hipPosition;
		private RaycastHit hitInfo;
		public LayerMask groundLayerMask;
		public Vector3 footPositionOffset;
		public Vector3 footRotationOffset;
		
		void Update () {
			if(Input.GetKeyDown(KeyCode.E) && pickUpTarget)
			{
				animator.SetTrigger("PickUp");
			}
		}
		
		void OnAnimatorIK(int layerIndex)
		{
			if(pickUpTarget)
			{
				pickUpTargetWeight = animator.GetFloat("PickUpWeight");
				
				animator.SetIKPosition( AvatarIKGoal.RightHand, pickUpTarget.GetComponent<Info_PickUp>().rightHandTarget.position );
				animator.SetIKPositionWeight( AvatarIKGoal.RightHand, pickUpTargetWeight);
				
				animator.SetIKRotation( AvatarIKGoal.RightHand, pickUpTarget.GetComponent<Info_PickUp>().rightHandTarget.rotation);
				animator.SetIKRotationWeight ( AvatarIKGoal.RightHand, pickUpTargetWeight);
				
				animator.SetLookAtPosition(pickUpTarget.transform.position);
				animator.SetLookAtWeight(
					1 - Mathf.Clamp01( Mathf.InverseLerp(0, GetComponent<CapsuleCollider>().radius,
					Vector2.Distance( new Vector2(transform.position.x,transform.position.z) , 
					new Vector2(pickUpTarget.transform.position.x, pickUpTarget.transform.position.z) ) ) )
					,0.2f,1,0,1);
			}
			else
			{
				animator.SetLookAtWeight(0,0,0,0,0);
			}
			
			if(Physics.Raycast( hipPosition[0].position, Vector3.down, out hitInfo, groundLayerMask))
			{
				animator.SetIKPosition( AvatarIKGoal.RightFoot, hitInfo.point + footPositionOffset);
				animator.SetIKPositionWeight( AvatarIKGoal.RightFoot, 1);
				
				animator.SetIKRotation( AvatarIKGoal.RightFoot, Quaternion.LookRotation( hitInfo.normal + footRotationOffset ) );
				animator.SetIKRotationWeight( AvatarIKGoal.RightFoot, 1);
			}
			else
			{
				animator.SetIKPositionWeight( AvatarIKGoal.RightFoot, 0);
				animator.SetIKRotationWeight( AvatarIKGoal.RightFoot, 1);
			}
		}
		
		void OnTriggerEnter(Collider other)
		{
			if(other.gameObject.CompareTag("PickUpable"))
			{
				pickUpTarget = other.gameObject;
			}
		}
		
		void OnTriggerExit(Collider other)
		{
			if(other.gameObject.CompareTag("PickUpable"))
			{
				pickUpTarget = null;
			}
		}
	}
}












