﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Uniat
{
	public class PlayerController : UniatBehaviour
	{
		[Header("Character State")]
		public BodyController currentBodyController;
		
		[Header("Character Objects")]
		public GameObject m_physicalController;
		public GameObject m_visuallController;
		public SensesController m_sensesController;
		
		
		[Header("Controllers")]
		public CharacterController m_cc;
		public Animator m_anim;
	
		[Header("Movement Settings")]
		public float speed;
		//public float gravity;
		public float jump;
		
		//TODO This should be managed by an external script
		//TODO All Input Should be removed from this code
		private Vector3 direction;
		
		public enum BodyController
		{
			physical,
			visual,
		}
	
		void Start ()
		{
			if(m_cc==null)
				m_cc = m_physicalController.GetComponent<CharacterController>();
		}
		
		void Update () 
		{
			switch (currentBodyController)
			{
			case BodyController.physical: 
				{
					CharacterMovement();
			}break;
				
			}
		}
		
		public void CharacterMovement()
		{
			//TODO if for OnAirMovement
			direction = new Vector3( Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
			
			//Jump Logic
			if(m_sensesController.isGrounded)
			{
				if(Input.GetKeyDown(KeyCode.Space))
				{
					direction +=(Vector3.up*jump);
				}
			}
			
			//TODO convertir direccion relativa al frente del personaje
			direction *= speed;
			
			//Apply Gravity
			ApplyGravity();
			
			m_cc.Move(direction * Time.deltaTime);
		}
		
		public void ApplyGravity()
		{
			direction += (Physics.gravity);	
			return;
			/*
			if(m_sensesController.isGrounded)
			{
				direction.y = 0;
			}
			else
			{
				direction -= (Vector3.up*gravity);	
			}
			*/
		}
		
		void LateUpdate()
		{
			MatchCurrentController();
		}
		
		public void MatchCurrentController()
		{
			switch(currentBodyController)
			{
			case BodyController.physical: 
				{
					m_visuallController.transform.position = m_physicalController.transform.position;
					m_sensesController.gameObject.transform.position = m_physicalController.transform.position;
				} break;
			case BodyController.visual: 
				{
					//TODO, too complex for now
					m_physicalController.transform.position = m_visuallController.transform.position;
					m_sensesController.gameObject.transform.position = m_visuallController.transform.position;
					//m_visuallController.transform.position = m_physicalController.transform.position;
				} break;
			}
		}
		
		public void SwitchCurrentController(BodyController _newController)
		{
			currentBodyController = _newController;
			switch(currentBodyController)
			{
			case BodyController.physical: 
				{
					m_anim.applyRootMotion = false;
				} break;
			case BodyController.visual: 
				{
					m_anim.applyRootMotion = true;
				} break;
			}
		}
	}
}
















