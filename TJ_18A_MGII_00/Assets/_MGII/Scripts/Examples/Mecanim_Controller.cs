﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Uniat
{
	public class Mecanim_Controller : MonoBehaviour 
	{
		private Animator animator;
		private Renderer renderer;
		private float _speed;
		public float moveSpeed = 10;
		public float speed
		{
			get {return _speed;}			
			set {
				_speed = value;
				animator.SetFloat("Speed",value);
			}
		}
		
		void Start () 
		{
			animator = GetComponent<Animator>();
			renderer = GetComponent<Renderer>();
		}
		
		public void SetColor(Color _newColor)
		{
			renderer.material.color = _newColor;
		}
		
		public Vector2 GetDirection()
		{
			Vector2 _direction = new Vector2(
				Input.GetAxis("Horizontal"),
				Input.GetAxis("Vertical"));
			_direction.Normalize();
			speed = _direction.magnitude;
			return _direction;
		}
	}
}

