﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Uniat
{
	public class SensesController : UniatBehaviour
	{
		public float m_topHeight = 2;
		public int m_numberOfRays = 10;
		public LayerMask m_groundMask;
		public LayerMask m_climbMask;
		private RaycastHit m_hitInfo;
	
		private float m_raySteps;
		private Vector3 m_rayPosition;
		public float m_rayLenght = 1;
		public float m_groundRayLenght = 0.2f;
		
		public bool[] rayHits;
		
		public AnimationCurve climbBoxMask;
		public AnimationCurve hangOnEdgeMask;
		
		public bool isGrounded;
		
		public bool CanClimbBox()
		{
			bool canDoIt = true;
			if(isGrounded)
			{
				for(int i=0; i< m_numberOfRays; i++)
				{
					float evaluationTime = Mathf.InverseLerp(0,m_numberOfRays-1,i);
					
					if(climbBoxMask.Evaluate(evaluationTime) > 0.5f )
					{
						if(rayHits[i]==false)
						{
							canDoIt = false;
						}
					}
					else
					{
						if(rayHits[i]==true)
						{
							canDoIt = false;
						}
					}
				}
			}
			else
			{
				canDoIt = false;
			}
			return canDoIt;
		}
		
		//Wall is different to a single edge
		public bool CanHangOnWall()
		{
			bool canDoIt = true;
			if(!isGrounded)
			{
				for(int i=0; i< m_numberOfRays; i++)
				{
					float evaluationTime = Mathf.InverseLerp(0,m_numberOfRays-1,i);
					
					if(hangOnEdgeMask.Evaluate(evaluationTime) == 1.0f )
					{
						if(rayHits[i]==false)
						{
							canDoIt = false;
						}
					}
					else if(hangOnEdgeMask.Evaluate(evaluationTime) > 0.0f && hangOnEdgeMask.Evaluate(evaluationTime) < 1.0f )
					{
						/*
						if(rayHits[i]==false)
						{
							canDoIt = false;
						}
						*/
					}
					else
					{
						if(rayHits[i]==true)
						{
							canDoIt = false;
						}
					}
				}
			}
			else
			{
				canDoIt = false;
			}
			return canDoIt;
		}
		
		
		
		void Start () 
		{
			m_raySteps = m_topHeight/m_numberOfRays;
			rayHits = new bool[m_numberOfRays];
		}
		
		void Update () 
		{
			//Forward Senses (For Climbing mainly)
			for(int i=0; i<m_numberOfRays; i++)
			{
				m_rayPosition =transform.position+(Vector3.up*i*m_raySteps);
				rayHits[i] = Physics.Raycast(m_rayPosition, transform.forward,out m_hitInfo, m_rayLenght, m_climbMask );
				//*
				if(rayHits[i])
				{
					Debug.DrawLine(m_rayPosition,m_hitInfo.point, Color.red);
				}
				else
				{
					Debug.DrawRay(m_rayPosition,Vector3.forward*m_rayLenght, Color.green);
				}
				//*/
			}
			
			//Downward Senses (For Grounded)
			isGrounded = (Physics.Raycast((transform.position + (Vector3.up*0.1f)), Vector3.down, m_groundRayLenght, m_groundMask ));
			Debug.DrawRay((transform.position + (Vector3.up*0.1f)),Vector3.down * m_groundRayLenght, isGrounded? Color.red : Color.green);
			
			/*
			if(CanHangOnWall())
			{
				Debug.Log("Can Hang On Wall");
			}
			if(CanClimbBox())
			{
				Debug.Log("Can Climb a Box");
			}
			//*/
		}
		
		
	}
}





















