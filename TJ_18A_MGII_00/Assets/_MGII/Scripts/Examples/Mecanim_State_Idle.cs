﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Uniat
{
	public class Mecanim_State_Idle : StateMachineBehaviour
	{
		private Mecanim_Controller myController;
		
		public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			myController = animator.gameObject.GetComponent<Mecanim_Controller>();
			myController.SetColor(Color.blue);
		}
		
		public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			myController.GetDirection();
		}
	
	}

}