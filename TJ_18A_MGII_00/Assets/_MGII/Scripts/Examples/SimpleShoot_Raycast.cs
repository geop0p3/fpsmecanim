﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Uniat
{
	public class SimpleShoot_Raycast : UniatBehaviour
	{
		public GameObject decalPrefab;
		private Transform lastDecal;
		
		public float force;
		public Transform spawnPoint;

		private RaycastHit hitInfo;
		
		void Start () 
		{
			PoolManager.MakePool(decalPrefab,10,5,true);
		}
		
		void Update () 
		{
			if(Input.GetKeyDown(KeyCode.Mouse0))
			{
				if(Physics.Raycast(spawnPoint.position, spawnPoint.forward, out hitInfo))
				{
					
					lastDecal = PoolManager.Spawn(decalPrefab,
						hitInfo.point,
						Quaternion.LookRotation(hitInfo.normal));
						
					if(lastDecal)
					{
						lastDecal.parent = hitInfo.transform;
					}
				}
			}
		}

	}
}

