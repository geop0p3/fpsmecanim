﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Uniat
{

	public class Mecanim_State_Move : StateMachineBehaviour 
	{
		private Mecanim_Controller myController;
		
		public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			myController = animator.gameObject.GetComponent<Mecanim_Controller>();
			myController.SetColor(Color.green);
		}
		
		public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			myController.gameObject.transform.Translate( 
				new Vector3(
				myController.GetDirection().x,
				0,
				myController.GetDirection().y
				) * Time.deltaTime * myController.moveSpeed );
		}
	}
}