﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Uniat.FPS
{
	public class FPS_Controller_Weapons : UniatBehaviour
	{
		[Header("Weapons Controller Settings")]
		public List<FPS_Weapon> myWeapons;
		public int currentWeaponID;
		private bool isChangingWeapon;
		private bool weaponLoaded;
		
		public void PreviousWeapon()
		{
			if(!isChangingWeapon)
			{
				StartCoroutine(ChangeWeapon(false));	
			}
		}
		
		public void NextWeapon()
		{
			if(!isChangingWeapon)
			{
				StartCoroutine(ChangeWeapon(true));
			}
		}
		
		public void SelectWeapon(int newWeaponID)
		{
			Debug.Log("Trying To Select weapon " + newWeaponID);
			if(!isChangingWeapon)
			{
				StartCoroutine(ChangeWeapon(newWeaponID));
			}
		}
		
		IEnumerator ChangeWeapon(int newID)
		{
			Debug.Log("Validating " + newID);
			if(newID != currentWeaponID)
			{
				if(newID >=0 && newID < myWeapons.Count)
				{
					Debug.Log("Lock Change Weapon");
					isChangingWeapon = true;
					UnloadWeapon(currentWeaponID);
			
					while(weaponLoaded){yield return null;}
			
					myWeapons[currentWeaponID].gameObject.SetActive(false);
				
					currentWeaponID = newID;
				
					LoadWeapon(currentWeaponID);	
					while (!weaponLoaded){yield return null;}
					Debug.Log("Release Change Weapon");
					isChangingWeapon = false;		
				}
				else
				{
					Debug.Log("Is Out of Range " + newID);
				}
			}
			else
			{
				Debug.Log("Is The Same As Current " + newID);	
			}
			yield return null;
		}
		
		IEnumerator ChangeWeapon(bool positive)
		{
			Debug.Log("Lock Change Weapon");
			isChangingWeapon = true;
			UnloadWeapon(currentWeaponID);
			
			while(weaponLoaded){yield return null;}
			
			myWeapons[currentWeaponID].gameObject.SetActive(false);
			
			if(positive)
			{
				currentWeaponID++;
				if(currentWeaponID >= myWeapons.Count) {currentWeaponID = 0;}
			}
			else
			{
				currentWeaponID--;
				if(currentWeaponID<0) { currentWeaponID = myWeapons.Count-1; }	
			}
			
			LoadWeapon(currentWeaponID);	
			while (!weaponLoaded){yield return null;}
			Debug.Log("Release Change Weapon");
			isChangingWeapon = false;
		}
		
		public void WeaponUnloaded()
		{
			weaponLoaded = false;
		}
		
		public void WeaponLoaded()
		{
			weaponLoaded = true;
		}
		
		public void UnloadWeapon(int weaponID)
		{
			WeaponLoaded();
			myWeapons[weaponID].OnUnload();
			//myWeapons[weaponID].gameObject.SetActive(false);
		}
		
		public void LoadWeapon(int weaponID)
		{
			WeaponUnloaded();
			myWeapons[weaponID].gameObject.SetActive(true);
			myWeapons[weaponID].OnLoad();
		}
		
		public void ReloadWeapon()
		{
			myWeapons[currentWeaponID].OnReload();
		}
		
		public void ShootDownWeapon()
		{
			myWeapons[currentWeaponID].OnShootDown();
		}
		
		public void ShootWeapon()
		{
			myWeapons[currentWeaponID].OnShoot();
		}
		
		public void ShootUpWeapon()
		{
			myWeapons[currentWeaponID].OnShootUp();
		}
		
		//TODO
		//PickUpWeapon
		//PickUpAmmo
		
	}
}
