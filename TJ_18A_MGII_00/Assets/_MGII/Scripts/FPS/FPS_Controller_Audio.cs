﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Uniat.FPS
{
	public enum SoundFXType
	{
		explosions,
		shoots,
		footsteps,
		impacts,
	}
	
	[System.Serializable]
	public struct SoundFXData
	{
		public SoundFXType sfxt;
		public int id;
		public Vector3 location;
		public bool alt;
	}
	
	public class FPS_Controller_Audio : UniatBehaviour
	{
		public static FPS_Controller_Audio instance;
		
		void Awake()
		{
			instance = this;	
		}
		
		public SoundFXSettings[] explosions;
		public SoundFXSettings[] shoots;
		public SoundFXSettings[] footsteps;
		public SoundFXSettings[] impacts;
		
		[System.Serializable]
		public class SoundFXSettings
		{
			public string name;
			public AudioSource soundSource;
			public AudioClip[] soundVariants;
			public AudioClip[] altSoundVariants;
			public bool onLocation;
			public bool changePitch;
			public float pitchOffset = 0.1f;
		}
		
		public void PlaySound(SoundFXData sfxd)
		{
			switch(sfxd.sfxt)			
			{
			case SoundFXType.shoots: 
				//Se elije el ID de disparo Actual, a su AudioSource, se le pide reproducir un sonido random, de sus variantes.
				if(sfxd.alt)
				{
					shoots[sfxd.id].soundSource.PlayOneShot( shoots[sfxd.id].altSoundVariants[Random.Range(0,shoots[sfxd.id].altSoundVariants.Length)]); 	
				}
				else
				{
					shoots[sfxd.id].soundSource.PlayOneShot( shoots[sfxd.id].soundVariants[Random.Range(0,shoots[sfxd.id].soundVariants.Length)]); 
				}
				break;
			case SoundFXType.impacts: 
				//Se elije el ID de disparo Actual, a su AudioSource, se le pide reproducir un sonido random, de sus variantes.
				if(sfxd.alt)
				{
					impacts[sfxd.id].soundSource.PlayOneShot( impacts[sfxd.id].altSoundVariants[Random.Range(0,impacts[sfxd.id].altSoundVariants.Length)]); 	
				}
				else
				{
					impacts[sfxd.id].soundSource.PlayOneShot( impacts[sfxd.id].soundVariants[Random.Range(0,impacts[sfxd.id].soundVariants.Length)]); 
				}
				break;
			}
			
			
		}
		
	}
}



















