﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Uniat.FPS
{
	public class FPS_Controller_Player : UniatBehaviour
	{
		public FPS_Controller_Weapons weaponController;
		
		void Start()
		{
			for(int i=0; i<weaponController.myWeapons.Count; i++)	
			{
				weaponController.UnloadWeapon(i);
			}
			weaponController.LoadWeapon(0);
		}
		
		void Update()
		{
			//Inputs			
			//Main Action
			if(Input.GetMouseButtonDown(0))	{ weaponController.ShootDownWeapon(); }
			if(Input.GetMouseButton(0))		{ weaponController.ShootWeapon(); }
			if(Input.GetMouseButtonUp(0))	{ weaponController.ShootUpWeapon(); }
			//TODO Secondary Action ( Button (1) )
			
			//Change Weapons
			if(Input.GetKeyDown(KeyCode.Q)) {weaponController.PreviousWeapon();}
			if(Input.GetKeyDown(KeyCode.E)) {weaponController.NextWeapon();}
			if(Input.GetAxisRaw("Mouse ScrollWheel") > 0){weaponController.NextWeapon();}
			if(Input.GetAxisRaw("Mouse ScrollWheel") < 0){weaponController.PreviousWeapon();}
			
			if(Input.GetKeyDown(KeyCode.Alpha1)) {weaponController.SelectWeapon(0);}
			if(Input.GetKeyDown(KeyCode.Alpha2)) {weaponController.SelectWeapon(1);}
			if(Input.GetKeyDown(KeyCode.Alpha3)) {weaponController.SelectWeapon(2);}
			if(Input.GetKeyDown(KeyCode.Alpha4)) {weaponController.SelectWeapon(3);}
			if(Input.GetKeyDown(KeyCode.Alpha5)) {weaponController.SelectWeapon(4);}
			if(Input.GetKeyDown(KeyCode.Alpha6)) {weaponController.SelectWeapon(5);}
			if(Input.GetKeyDown(KeyCode.Alpha7)) {weaponController.SelectWeapon(6);}
			if(Input.GetKeyDown(KeyCode.Alpha8)) {weaponController.SelectWeapon(7);}
			if(Input.GetKeyDown(KeyCode.Alpha9)) {weaponController.SelectWeapon(8);}
			if(Input.GetKeyDown(KeyCode.Alpha0)) {weaponController.SelectWeapon(9);}
			
			
			if(Input.GetKeyDown(KeyCode.R)){ weaponController.ReloadWeapon();}
		}
		
		
	}
}
