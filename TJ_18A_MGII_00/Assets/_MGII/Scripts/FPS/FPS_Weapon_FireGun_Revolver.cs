﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Uniat.FPS
{
	public class FPS_Weapon_FireGun_Revolver : FPS_Weapon_FireGun
	{
		//[Header("Revolver Settings")]
		
		private RaycastHit hitInfo;
		
		//Temporal, esto no deberia de ir aqui
		[Header("Decal Settings")]
		public GameObject decalPrefab;
		private Transform lastDecal;
		private Transform lastShell;
		
		[Header("FX Settings")]
		public GameObject shellPrefab;
		public Transform[] shellSpawnPoints;
		public SoundFXData successfulShootSFXD;
		public SoundFXData emptyShootSFXD;
		
		void Start()
		{
			PoolManager.MakePool(decalPrefab,12,6,true);
			PoolManager.MakePool(shellPrefab,18,6,true);
		}
		
		public override void OnShootDown()
		{
			if( OnFire() )
			{
				SetShootSuccess(true);
				base.OnShootDown();
				//ExcecuteShoot();
			}
			else
			{
				SetShootSuccess(false);
				//TODO should be called from animation
				FPS_Controller_Audio.instance.PlaySound( emptyShootSFXD );
				base.OnShootDown();
			}
		}
		
		public override void OnShoot(){}
		
		public override void OnShootUp(){}
		
		public override void ExcecuteShoot()
		{
			if(Physics.Raycast(spawnPoint.position, spawnPoint.forward, out hitInfo))
			{
				FPS_Controller_Audio.instance.PlaySound( successfulShootSFXD );
				
				lastDecal = PoolManager.Spawn(decalPrefab, hitInfo.point, Quaternion.LookRotation( hitInfo.normal));
				lastDecal.parent = null;
				lastDecal.localScale = Vector3.one;
				lastDecal.parent = hitInfo.transform;
			}
		}
		
		public void InstantiateOldBullets()
		{
			for(int i=0; i<shellSpawnPoints.Length;i++)
			{
				lastShell = PoolManager.Spawn(shellPrefab,shellSpawnPoints[i].position, shellSpawnPoints[i].rotation);
			}
		}
		
	}
}