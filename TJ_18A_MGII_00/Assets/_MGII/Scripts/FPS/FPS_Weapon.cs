﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Uniat.FPS
{
	public class FPS_Weapon : UniatBehaviour
	{
		[Header("Common Weapon Settings")]
		public FPS_Controller_Weapons myWeaponController;
		public float damage;
		public Animator animController;
		public bool coolDown;
		
		public virtual void OnShootDown()
		{
			//Debug.Log("Disparo Vacio DOWN");
			animController.SetTrigger("shootDown");
		}
		
		public virtual void OnShoot()
		{
			//Debug.Log("Disparo Vacio");
		}
		
		
		public virtual void OnShootOn()
		{
			//Debug.Log("Disparo Vacio");
			animController.SetBool("shooting", true);
		}
		
		public virtual void OnShootOff()
		{
			//Debug.Log("Disparo Vacio");
			animController.SetBool("shooting", true);
		}
		
		public virtual void OnShootUp()
		{
			//Debug.Log("Disparo Vacio UP");
			animController.SetTrigger("shootUp");
		}
		
		public virtual void OnLoad()
		{
			//Debug.Log("Load Vacio");
			animController.SetBool("isLoaded", true);
		}
		
		public virtual void OnReload()
		{
			//Debug.Log("Reload Vacio");
			animController.SetTrigger("reload");
		}
		
		public virtual void OnUnload()
		{
			//Debug.Log("Unload Vacio");
			animController.SetBool("isLoaded", false);
		}
		
		public virtual void SetShootSuccess(bool wasSuccess)
		{
			animController.SetBool("shootSuccesful", wasSuccess);
		}
		
		public virtual void ExcecuteShoot()
		{
			
		}
		
		//TODO tal vez sea mas eficiente tener una propiedad bool y desde el WeaponController 
		//revisar si la propiedad es true o false (en lugar de esto y la variable en WC)
		public virtual void WeaponLoaded()
		{
			myWeaponController.WeaponLoaded();
		}
		
		public virtual void WeaponUnloaded()
		{
			myWeaponController.WeaponUnloaded();
		}
		
	}
}
