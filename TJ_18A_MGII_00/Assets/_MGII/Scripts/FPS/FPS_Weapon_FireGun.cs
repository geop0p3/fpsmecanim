﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Uniat.FPS
{
	public class FPS_Weapon_FireGun : FPS_Weapon
	{
		[Header("Common FireGun Settings")]
		public Transform spawnPoint;
		public int totalAmmo;
		public int magazineSize;
		public int currentMagazineCount;
		
		public override void OnShootDown()
		{
			base.OnShootDown();
		}
		
		public override void OnShoot()
		{
			base.OnShoot();
		}
		
		public override void OnShootUp()
		{
			base.OnShootUp();
		}
		
		public override void OnReload()
		{
			//TODO, si no tienes nada para cargar, hace animacion?
			base.OnReload();
			if(totalAmmo>0)
			{
				if(totalAmmo > (magazineSize-currentMagazineCount))
				{
					totalAmmo -= (magazineSize-currentMagazineCount);
					currentMagazineCount = magazineSize;
				}
				else
				{
					currentMagazineCount += totalAmmo;
					totalAmmo = 0;
				}
			}
		}
		
		public virtual bool OnFire()
		{
			if(currentMagazineCount>0)
			{
				currentMagazineCount--;
				return true;
			}
			else
			{
				return false;
			}
		}
		
		
	}
}