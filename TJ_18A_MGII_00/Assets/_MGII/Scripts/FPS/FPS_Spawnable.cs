﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Uniat.FPS
{
	public class FPS_Spawnable : UniatBehaviour
	{
		[Header("Main Settings")]
		public Rigidbody rbd;
		
		[Header("Self Destruct Settings")]
		public bool selfDestruct;
		public bool destroyOnCollision;
		public float life = 3;
		
		[Header("FX Settings")]
		public bool soundOnCollision;
		public SoundFXData soundFXD;
	
		public void OnSpawn()//Nuevo Start Fake
		{
			if(selfDestruct)
			Invoke("SelfDestruct",life);
		}
		
		void SelfDestruct()
		{
			//Ya no se llama a un Destroy, ahora reciclamos llamando a Despawn
			PoolManager.Despawn(gameObject);
		}
		
		
		public void OnDespawn()//Codigo seguro para dejar la instancia limpia y lista para ser utilizado de nuevo
		{
			CancelInvoke();
			if(rbd)rbd.velocity = Vector3.zero;
		}		
		
		void OnCollisionEnter(Collision collisionInfo)
		{
			if(soundOnCollision)	
			{
				FPS_Controller_Audio.instance.PlaySound(soundFXD);
			}
		}
	}
}
