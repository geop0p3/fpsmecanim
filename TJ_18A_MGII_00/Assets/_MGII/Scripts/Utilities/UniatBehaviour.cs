﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Uniat
{
	public class UniatBehaviour : MonoBehaviour 
	{
	
		public void SetPosition_X(float newX)
		{
			transform.position = new Vector3(newX,
				transform.position.y,
				transform.position.z);
		}
	
	}
}