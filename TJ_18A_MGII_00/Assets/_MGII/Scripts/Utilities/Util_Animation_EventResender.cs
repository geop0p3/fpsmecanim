﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Uniat
{
	public class Util_Animation_EventResender : UniatBehaviour
	{	
		public GameObject listener;
		
		 public void CatchEvent(string eventName)
		 {
		 	listener.SendMessage(eventName, SendMessageOptions.DontRequireReceiver);
		 }
		 
		public UnityEvent eventToCall;
		
		public void CallEvent()
		{
			eventToCall.Invoke();
		}
		
		public void ResendMessage(string messageToSend)
		{
			listener.SendMessage(messageToSend, SendMessageOptions.DontRequireReceiver);
		}
	}
}