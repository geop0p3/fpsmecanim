﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Uniat
{
	public class Util_Animator_Bool : Util_Animator_Variable
	{
		[Header("Behaviours")]
		public BoolBehaviours boolBehaviour;
		
		[Header("Values")]
		public bool enterValue;
		public bool exitValue;
		
		public enum BoolBehaviours
		{
			setValue,
			toggleValue
		}
		
		public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			if(!isSubMachine)
			{
				if(isEnter)
				{
					BoolLogic(animator,enterValue);
				}
			}
		}
		
		public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			if(!isSubMachine)
			{
				if(isExit)
				{
					BoolLogic(animator,exitValue);
				}
			}
		}
		
		public override void OnStateMachineEnter(Animator animator, int stateMachinePathHash)
		{
			if(isSubMachine)
			{
				if(isEnter)
				{
					BoolLogic(animator,enterValue);
				}
			}
		}
		
		public override void OnStateMachineExit(Animator animator, int stateMachinePathHash)
		{
			if(isSubMachine)
			{
				if(isExit)
				{
					BoolLogic(animator,exitValue);
				}
			}
		}
		
		private void BoolLogic(Animator animator, bool boolValue)
		{
			switch(boolBehaviour)
			{
			case BoolBehaviours.setValue: 
				{
					animator.SetBool(variableName,boolValue);
				} break;
			case BoolBehaviours.toggleValue: 
				{
					animator.SetBool(variableName,!animator.GetBool(variableName));
				} break;
			}
		}
		
	}//End Class
}//End Namespace