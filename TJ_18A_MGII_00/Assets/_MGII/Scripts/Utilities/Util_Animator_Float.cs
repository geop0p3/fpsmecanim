﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Uniat
{
	public class Util_Animator_Float : Util_Animator_Variable
	{
	public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			if(!isSubMachine)
			{
				if(isEnter)
				{
					
				}
			}
		}
		
		public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			if(!isSubMachine)
			{
				if(isExit)
				{
					
				}
			}
		}
		
		public override void OnStateMachineEnter(Animator animator, int stateMachinePathHash)
		{
			if(isSubMachine)
			{
				if(isEnter)
				{
					
				}
			}
		}
		
		public override void OnStateMachineExit(Animator animator, int stateMachinePathHash)
		{
			if(isSubMachine)
			{
				if(isExit)
				{
					
				}
			}
		}

	}
}