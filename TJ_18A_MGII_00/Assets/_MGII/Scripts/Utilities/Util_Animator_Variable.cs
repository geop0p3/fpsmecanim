﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Uniat
{
	public class Util_Animator_Variable : StateMachineBehaviour
	{
		[Header("State Info")]
		public string variableName = "";
		public bool isSubMachine;
		
		[Header("Events")]
		public bool isEnter;
		public bool isExit;
	}
}