﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Uniat
{
	//-------- Clase que solo existe una instancia de ella 
	public class MySingleton : UniatBehaviour
	{
		//--------------------------------------------------------------------------------------------------- SINGLETON
		 //Variable estatica, que va a guardar la unica instancia de este script
		 private static MySingleton m_instance;
		 
		 //Propiedad Publica para acceder a la instancia del script
		 public static MySingleton Instance
		 {
		 	//No Setter
		 	
		 	//Getter
		 	get
		 	{
		 		//Al solicitar la instancia, verificar si no existe
		 		if(m_instance==null)
		 		{
		 			//Si no existe la instancia se crea un GO y se le asigna el script
		 			GameObject go = new GameObject("My Singleton");
		 			//Al crearse el script, se va a llamar el Awake
		 			go.AddComponent<MySingleton>();
		 			//En este momento Se llama el Awake Automaticamente
		 			
		 			DontDestroyOnLoad( go );
		 		}
		 		return m_instance;
		 	}
		 }
		 
		//Esto al ser un MonoBehaviour (no tiene constructor), se utiliza Awake
		 //Al llamarse el script, se asigna la instancia actual en la variable estatica
		 void Awake()
		 {
		 	if(m_instance)
		 	{
		 		//Destroy(gameObject);
		 		Debug.LogWarning("Deleting Second Singleton Instance");
		 		Destroy(this);
		 	}
		 	else
		 	{
		 		Debug.Log("Created Singleton Instance");
			 	m_instance = this;	
		 	}
		 	
		 	 
		 }
		 
		//--------------------------------------------------------------------------------------------------- Subscribeable Events
		
		//Metodo que no hace nada solo para forzar a que exista el singleton
		public void Initialize()
		{
			//Do Nothing YOLO
		}
		
		//Delegado
		//Template del delegado, describe la estructura base de un metodo (Con inputs y outputs)
		public delegate void IntTemplate(int newScore);
		
		//Variable del evento del delegado, es como un metodo, del tipo de tu Delegado, pero no hace nada, es como una combinacion entre variable y metodo
		public static event IntTemplate addScoreHandler;
		public static event IntTemplate damagePlayerHandler;
		
		public void AddNewScore(int newScore)
		{
			addScoreHandler(newScore);
		}
		
		public void DamagePlayer(int newDamage)
		{
			damagePlayerHandler(newDamage);
		}
	}
}
















