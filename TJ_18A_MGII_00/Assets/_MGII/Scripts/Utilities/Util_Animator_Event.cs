﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Uniat
{
	public class Util_Animator_Event : StateMachineBehaviour
	{
		[Header("State Info")]
		public string methodToCall = "";
		public bool isSubMachine;
		
		[Header("Events")]
		public bool isEnter;
		public bool isExit;
		
		private void SendTheMessage(Animator animator)
		{
			animator.gameObject.GetComponent<Util_Animation_EventResender>().ResendMessage(methodToCall);
		}
		
		public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			if(!isSubMachine)
			{
				if(isEnter)
				{
					SendTheMessage(animator);
				}
			}
		}
		
		public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			if(!isSubMachine)
			{
				if(isExit)
				{
					SendTheMessage(animator);
				}
			}
		}
		
		public override void OnStateMachineEnter(Animator animator, int stateMachinePathHash)
		{
			if(isSubMachine)
			{
				if(isEnter)
				{
					SendTheMessage(animator);
				}
			}
		}
		
		public override void OnStateMachineExit(Animator animator, int stateMachinePathHash)
		{
			if(isSubMachine)
			{
				if(isExit)
				{
					SendTheMessage(animator);
				}
			}
		}
	}
}