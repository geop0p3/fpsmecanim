﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Uniat
{
	public class Input_Movement : UniatBehaviour
	{
		public float speed;
		public Space relativeTo = Space.World;
		
		void Update () 
		{
			transform.Translate(new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"),0)*speed*Time.deltaTime, relativeTo);
			
		}
	}
}